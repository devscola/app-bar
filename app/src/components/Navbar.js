import React from 'react'
import '../../styles/navbar.css'
import { Link } from 'react-router-dom'


const Navbar = () => {

    return (
        <div className="navbar-container">
            <Link to="/Menu" className="navbar-option">Menú</Link>
            <Link to="/Products" className="navbar-option">Productos</Link>
            <Link to="/Tables" className="navbar-option">Mesas</Link>
        </div>
    )
}

export default Navbar