import React, { useEffect, useState } from 'react'
import '../../styles/styles.css'
import ApiClient from '../infraestructure/ApiClient'

const Tables = ({ user_id }) => {
    const [new_table, setNewTable] = useState('')
    const [tables, setTables] = useState([])
    const [selected_table, setSelectedTable] = useState('')

    useEffect(() =>{
        getTables()
    }, [])

    const getTables = async () => {
        const response = await ApiClient.getJson(`get_tables?user_id=${user_id}`)
        setTables(response)
    }

    const handleInput = (event) => {
        setNewTable(event.target.value)
    }

    const handleOnKeyDown = (event) => {
         if(event.keyCode == 13) {
            addTable()
         }
    }

    const addTable = async () => {

        if(tables.includes(new_table)) {
            return alert('Ya existe la mesa')
        }

        const data = {user_id: user_id, table_name: new_table}

        const response = await ApiClient.postJson('add_table', data)
        setTables(response)
        setNewTable('')
    }
    
    const deleteSelectedTable = async () => {
        const data = {user_id: user_id, table_name: selected_table}

        const response = await ApiClient.postJson('delete_table', data)
        setTables(response)
    }

    const selectTable = (event) => {
        setSelectedTable(event.target.value)
    }

    return (
        <div className="tablesContainer">
            <div className="title">MESAS</div>
            <div className="listContainer">
                <select name="tablesSelect" className="listElements" size="15">
                    {tables.map((table, index) =>{
                        return(<option key={index} onClick={selectTable}>{table}</option>)
                    })}
                </select>
            </div>
            <div className="inputContainer">
                <input type="text" name="tableName" className="inputText" 
                onChange={handleInput} 
                value={new_table} 
                onKeyDown={handleOnKeyDown}/>
            </div>
            <div className="buttonsContainer">
                <button className="button" onClick={addTable}>Añadir</button>
                <button onClick={deleteSelectedTable}>Eliminar</button>
            </div>
        </div>
    )
}

export default Tables