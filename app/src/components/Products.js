import React, { useEffect, useState } from 'react'
import ApiClient from '../infraestructure/ApiClient'
import '../../styles/styles.css'


const Products = ({user_id}) => {

    const [new_category, setNewCategory] = useState('')
    const [new_product, setNewProduct] = useState('')
    const [categories, setCategories] = useState([])
    const [products, setProducts] = useState([])
    const [selected_category, setSelectedCategory] = useState('')
    const [selected_product, setSelectedProduct] = useState('')

    const handleInput = (event, option) => {
        if(option == 'category') {
          return setNewCategory(event.target.value)
        }
        setNewProduct(event.target.value)
    }
    
    useEffect(() =>{
        getCategories()
    }, [])

    useEffect(() =>{
        getProducts()
    }, [selected_category])

    const getCategories = async () => {
        const response = await ApiClient.getJson(`get_categories?user_id=${user_id}`)
        setCategories(response)
    }

    const getProducts= async () => {
        const response = await ApiClient.getJson(`get_products?user_id=${user_id}&category=${selected_category}`)
        let products = []
        response.map((product, index) => {
            products.push(product.product_name)
        })

        setProducts(products)
    }

    const handleOnKeyDown = (event) => {
        if(event.keyCode == 13) {
           handleAdd()
        }
    }
    
    const handleAdd = async (option) => {
        if(option == 'category') {
            if(categories.includes(new_category)) {
                return alert('Ya existe la categoria')
            }
            const data = {user_id: user_id, category_name: new_category}

            const response = await ApiClient.postJson(`add_category`, data)
            
            setCategories(response)
            setNewCategory('')
            return 
        }
        
        if(products.includes(new_product)) {
                return alert('Ya existe el producto')
            }
            
            const data = {
                user_id: user_id,
                product_name: new_product,
                category_name: selected_category
            }

            const response = await ApiClient.postJson(`add_product`, data)

            let products_elemets = []
            response.map((product, index) => {
                products_elemets.push (product.product_name)
            })
            setProducts(products_elemets)
            setNewProduct('')
            return 
    }

    const delete_cat = async (id_user, category_selected) => {
        const data = {user_id: id_user, category_name : category_selected}
        const response = await ApiClient.postJson('delete_category', data)
        getCategories()
    }

    const handleDelete = async (option) => {
        if(option == 'category') {
            if(products.length > 0){
                if (confirm('Esta categoria contiene productos, ¿Desea borrarlos?')) {
                    delete_cat(user_id, selected_category)
                    
                    products.map( async (item) =>{
                        const data = {user_id: user_id, product_name : item}
                        const response = await ApiClient.postJson('delete_product', data)
                        getProducts()
                    })
                    return
                }
            }else{
                delete_cat(user_id, selected_category)
            }
        }
				
        const data = {user_id: user_id, product_name : selected_product}
        const response = await ApiClient.postJson('delete_product', data)
        getProducts()  
    }
				
   	const selectElement = (event, option) => {
        if(option == 'category') {
            setSelectedCategory(event.target.value)
            return
        }
        setSelectedProduct(event.target.value)
    }

    return (
        <div>
            <div className="tablesContainer">
                <div className="title">CATEGORIAS</div>
                <div className="listContainer">
                    <select className="listElements" size="15">
                    {categories.map((category, index) =>{
                        return(<option key={index} onClick={(event) => selectElement(event, 'category')}>{category}</option>)
                    })}
                    </select>
                </div>
                <div className="inputContainer">
                    <input className="inputText" type="text" value={new_category} onChange={(event) => handleInput(event, 'category')}/>
                </div>
                <div className="buttonsContainer">
                    <button className="button" onClick={() => handleAdd('category')}>Añadir</button>
                    <button className="button" onClick={() => handleDelete('category')}>Eliminar</button>
                </div>
            </div>
            <div className="tablesContainer">
                <div className="title">PRODUCTOS</div>
                <div className="listContainer">
                    <select className="listElements" size="15">
                    {products.map((product, index) =>{
                        return(<option key={index} onClick={selectElement}>{product}</option>)
                    })}
                    </select>
                </div>
                <div className="inputContainer">
                    <input className="inputText" type="text" value={new_product} 
                    onChange={(event) => handleInput(event, 'product')}
                    onKeyDown={handleOnKeyDown}/>
                </div>
                <div className="buttonsContainer">
                    <button className="button" onClick={() => handleAdd('product')}>Añadir</button>
                    <button className="button" onClick={() => handleDelete('prodcut')}>Eliminar</button>
                </div>
            </div>
        </div>
    )
}

export default Products