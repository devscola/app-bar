import React, { useState, useEffect } from 'react'
import ApiClient from '../infraestructure/ApiClient'
import Selector from './selector'
import '../../styles/conversacionesCss.css'

const Menu = ({user_id}) => {
    
  const [first_plates, setFirstPlates] = useState([])
  const [selected_first_plate, setSelectedFirstPlate]= useState('')    
  
  const [second_plates, setSecondPlates] = useState([])
  const [selected_second_plate, setSelectedSecondPlate]= useState('')    

  const [desserts, setDesserts] = useState([])
  const [selected_dessert, setSelectedDessert]= useState('')    
  
  const [others, setOthers] = useState([])
  const [selected_other, setSelectedOther]= useState('')  
  
  const [new_plate, setNewPlate] = useState({
    first_plate: '',
    second_plate: '',
    dessert: '',
    other: ''
  })

  useEffect(() =>{
      getMenu()
  }, [])

  const getMenu = async () => {
    const response = await ApiClient.getJson(`get_menu?user_id=${user_id}`)
    setFirstPlates(response[0].first_plates)
    setSecondPlates(response[0].second_plates)
    setDesserts(response[0].desserts)
    setOthers(response[0].others)

  }
  
  const handleInput = (event, option) => {
    if(option == 'first_plates') {
        return setNewPlate({...new_plate,first_plate:event.target.value})
    }

    if(option == 'second_plates') {
        return setNewPlate({...new_plate,second_plate:event.target.value})
    }

    if(option == 'desserts') {
        return setNewPlate({...new_plate,dessert:event.target.value})
    }

    setNewPlate({...new_plate,other:event.target.value})

  }

  const handleAdd = (option) => {

    addPlate(option, 'first_plates',first_plates, new_plate.first_plate, setFirstPlates, setNewPlate)
    
    addPlate(option, 'second_plates', second_plates, new_plate.second_plate, setSecondPlates, setNewPlate )
    
    addPlate(option, 'desserts', desserts, new_plate.dessert, setDesserts, setNewPlate )

    addPlate(option, 'others', others, new_plate.other, setOthers, setNewPlate )

  }  


  const addPlate = async (option, optionCheck, category, plate, setCategory, clearInput) => {
    if(option == optionCheck){
      if(category.includes(plate)) {
          return alert('Ya existe el plato')
      }
      
      let data = {user_id: user_id}
      data[optionCheck] = plate

      const response = await ApiClient.postJson('add_to_menu', data)
      setCategory(response)
      clearInput({...new_plate,
          first_plate:'',
          second_plate: '',
          dessert: '',
          other:''
      })
    }
  }



  const handleDelete = (option) => {
    if(option == 'first_plates') {
        return deleteFirstPlate()
    }

    if(option == 'second_plates') {
        return deleteSecondPlate()
    }

    if(option == 'desserts') {
        return deleteDessert()
    }

    deleteOthers()
  }

  const selectElement = (event, option) => {
    if(option == 'first_plates') {
        return setSelectedFirstPlate(event.target.innerHTML)
    } 

    if(option == 'second_plates') {
        return setSelectedSecondPlate(event.target.innerHTML)
    }

    if(option == 'desserts') {
        return setSelectedDessert(event.target.innerHTML)
    }

    setSelectedOther(event.target.innerHTML)
  }

  const deleteFirstPlate = async () => {
    const data = {user_id: user_id, first_plates: selected_first_plate}

    const response = await ApiClient.postJson('delete_item_menu', data)
    setFirstPlates(response)
  }

  const deleteSecondPlate = async () => {
    const data = {user_id: user_id, second_plates: selected_second_plate}

    const response = await ApiClient.postJson('delete_item_menu', data)
    setSecondPlates(response)
  }

  const deleteDessert = async () => {
    const data = {user_id: user_id, desserts: selected_dessert}

    const response = await ApiClient.postJson('delete_item_menu', data)
    setDesserts(response)
  }

  const deleteOthers = async () => {
    const data = {user_id: user_id, others: selected_other}
    const response = await ApiClient.postJson('delete_item_menu', data)
    setOthers(response)
  }

  return (
    <div id="menu">
        <Selector 
            title="PRIMER PLATO" 
            element_collection={first_plates} 
            selectElement={(event) => selectElement(event, 'first_plates')} 
            input_value={new_plate.first_plate}
            handleInput={(event) => handleInput(event, 'first_plates')}
            handleAdd={() => handleAdd('first_plates')}
            handleDelete={() => handleDelete('first_plates')}
        />
        <Selector 
            title="SEGUNDO PLATO" 
            element_collection={second_plates} 
            selectElement={(event) => selectElement(event, 'second_plates')} 
            input_value={new_plate.second_plate}
            handleInput={(event) => handleInput(event, 'second_plates')}
            handleAdd={() => handleAdd('second_plates')}
            handleDelete={() => handleDelete('second_plates')}
        />
        <Selector 
            title="POSTRE" 
            element_collection={desserts} 
            selectElement={(event) => selectElement(event, 'desserts')} 
            input_value={new_plate.dessert}
            handleInput={(event) => handleInput(event, 'desserts')}
            handleAdd={() => handleAdd('desserts')}
            handleDelete={() => handleDelete('desserts')}
        />
        <Selector 
            title="OTROS" 
            element_collection={others} 
            selectElement={(event) => selectElement(event, 'others')} 
            input_value={new_plate.other}
            handleInput={(event) => handleInput(event, 'others')}
            handleAdd={() => handleAdd('others')}
            handleDelete={() => handleDelete('others')}
        />
    </div> 
  )
}

export default Menu