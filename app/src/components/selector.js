import React from 'react'

const Selector =  ({
    title, 
    element_collection, 
    selectElement, 
    input_value, 
    handleInput, 
    handleOnKeyDown, 
    handleAdd, 
    handleDelete }) => {

    return(
        <div className="selectorContainer">
            <div className="title">{title}</div>
            <div className="listContainer">
                <ul className="listElements">
                    {element_collection.map((element, index) =>{
                        return(<li key={index} onClick={selectElement}>{element}</li>)
                    })}
                </ul>
            </div>
            <div className="inputContainer">
                <input className="inputText" 
                value={input_value} 
                onChange={handleInput} 
                onKeyDown={handleOnKeyDown}/>
            </div>
            <div className="buttonsContainer">
                <button onClick={handleAdd}>Añadir</button>
                <button onClick={handleDelete}>Eliminar</button>
            </div>
        </div>
    )
}

export default Selector