import React, { useState } from 'react'
import ApiClient from '../infraestructure/ApiClient'

const login =  ({setBar}) => {

    const [user, setUser] = useState({
        user_email: '',
        user_password: '',
        user_id: ''
      })
      
    const updateUserState = ( event ) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        })
    }

    const handleOnKeyDown = ( event ) => {
        if(event.keyCode == 13) doesUserExist()
        
    }

    const doesUserExist = async () => {
        const response = await ApiClient.postJson('check_user_exist', user)
        if (response.user){
            setBar({
                    user_id: response.user[0]._id,
                    bar_name: response.user[0].bar_name,
                    user_password: response.user[0].password,
                    user_email: response.user[0].email
            })
        } else {
            alert(response.error_message)
        }  
    }

    return (       
        <div className="login-container">
            <div className="input-container">
                <label>Introduce tu e-mail</label>
                <input
                    name="user_email"
                    className="e-mail" 
                    type="email" 
                    placeholder="tu E-mail@compañia.com"
                    onChange={updateUserState}
                ></input>
            </div>
            <div className="input-container">
                <label>Introduce tu contraseña</label>
                <input
                    name="user_password"
                    className="password"
                    type="password"
                    placeholder="tu contraseña"
                    onChange={updateUserState}
                    onKeyDown={handleOnKeyDown}
                >
                </input>

            </div>

            <div className="button-container">
                <button
                    className="button-login"
                    onClick={doesUserExist}>
                    login
                </button>
            </div>
        </div>
    )
}
 
export default login