import React, { useState, Fragment } from 'react'
import { BrowserRouter as  Router, Route, Switch, Link} from 'react-router-dom'
import Login from './components/login'
import Navbar from './components/Navbar'
import Tables from './components/Tables'
import Products from './components/Products'
import Menu from './components/Menu'

const App = () => {
  const [userData, setuserData] = useState({
    user_id: '',
    bar_name: '',
    user_password: '',
    user_email: ''
  })

  const {user_id, bar_name, user_password, user_email} = userData

  if(bar_name){
    return (
      <div>
        <Router>
            <Navbar/>
          <Switch>
            <Route path="/Menu" component={() =>  <Menu user_id={user_id}/>}>
            </Route>
            <Route path="/Products" component={() => <Products user_id={user_id}/>}>
            </Route>
            <Route path="/Tables" component={() => <Tables user_id={user_id}/>}>
            </Route>
          </Switch>
        </Router>
      </div>
    )
  } 

  return (
    <Login setBar={setuserData}/>
  )
}

export default App
