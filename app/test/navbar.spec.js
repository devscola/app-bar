import React from 'react';
import { render } from '@testing-library/react';
import Navbar from '../src/components/Navbar';


describe('Navbar', () => {
  it('renders the buttons', () => {
    const { getByText } = render(<Navbar />)
  
    getByText(/Menú/i)
    getByText(/Productos/i)
    getByText(/Mesas/i)
  })
})