import React from 'react';
import { render } from '@testing-library/react';
import App from '../src/App';


describe('App', () => {
  it('renders the initial text', () => {
    const { getByText } = render(<App />)
  
    getByText(/Logueate con tu cuenta de Google/i)
  })

  it('renders the Login button', () => {
    const { getByText } = render(<App />)
  
    getByText(/Login/i)
  })
})