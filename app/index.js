import React from "react"
import ReactDOM from "react-dom"
import App from "./src/App.js"

var mountNode = document.getElementById("app")
ReactDOM.render(<App />, mountNode || document.createElement('div'))
