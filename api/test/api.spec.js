const request = require('supertest')
const { application } = require('express')
const { json } = require('body-parser')
const host = 'http://localhost:3000'
describe('Api', ()=>{
  it('Is listening', (done)=>{
        request(host)
        .get('/')
        .set('Accept', 'text/html')
        .expect('Content-Type', /text/)
        .expect(200, done)    
    })

  xit('check the user exist',()=>{
    request(host)
    .post('/checkUserExist')
    .set('Content-Type', 'application/json')
  })  
})