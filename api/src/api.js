const express = require('express')
const app = express()
const cors = require('cors')
const port = process.env.PORT || 3000
const menus = require('./models/menus.js')
const products = require('./models/products.js')
const categories = require('./models/categories.js')
require('./Database')
const QRCode = require('qrcode')
const morgan = require('morgan')
const TablesService = require('./services/Tables')
const UsersService = require('./services/Users')
const tablesService = new TablesService()
const usersService = new UsersService()
const MenuService = require('./services/Menu')
const menuService = new MenuService()

app.use(express.json())
app.use(cors())
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

app.get('/', (req, res) => {
  res.send('BAR')
})

app.post('/check_user_exist', async (req, res) => {
  const email = req.body.user_email
  const password = req.body.user_password

  const result = await usersService.checkUserExist(email, password)

  res.json(result)
})

app.post('/add_table', async (req, res) => {
  const user_id = req.body.user_id
  const table_name = req.body.table_name
  const table_QR = `www.vercata.com/?${user_id}&table=${table_name}`

  const result = await tablesService.addTable(user_id, table_name, table_QR)

  res.json(result)
})

app.post('/delete_table', async (req, res) => {
  const user_id = req.body.user_id
  const table_name = req.body.table_name
  const table_QR = `www.vercata.com/?${user_id}&table=${table_name}`

  const result = await tablesService.deleteTable(user_id, table_name, table_QR)

  res.json(result)
})

app.get('/get_tables', async (req, res) => {
  const user_id = req.query.user_id

  const result = await tablesService.getTables(user_id)

  res.json(result)
})

app.post('/add_to_menu', async (req, res) => {
  const data = req.body
  const result = await menuService.addToMenu(data)

  res.json(result)
})

app.post('/delete_item_menu', async (req, res) => {
  const data = req.body
  const result = await menuService.deleteItemMenu(data)

  res.json(result)
})

app.get('/get_menu', async (req, res) => {
  const data = req.query.user_id
  const result = await menuService.getMenu(data)

  res.json(result)
})

app.post('/add_category', async (req, res) => {
  const user_id = req.body.user_id
  const product_name = req.body.product_name
  const category = req.body.category_name

  const categories_list = await categories.find({ user_id: user_id })

  if (categories_list.length < 1) {
    const new_categories = new categories({
      user_id: user_id,
      product_name: product_name,
      categories: category
    })
    new_categories.save()

    return res.json([category])
  }

  const updated_categories = await categories.findOneAndUpdate(
    { user_id: user_id },
    { $push: { categories: category } },
    { new: true })

  res.json(updated_categories.categories)

})

app.post('/delete_category', async (req, res) => {
  const user_id = req.body.user_id
  const category_name = req.body.category_name
  const updated_categories = await categories.findOneAndUpdate(
    { user_id: user_id },
    { $pull: { categories: category_name } },
    { new: true })
  res.json(updated_categories.categories)
})

app.get('/get_categories', async (req, res) => {
  const user_id = req.query.user_id

  const user_categories = await categories.find({ user_id: user_id })

  if (user_categories == undefined) {
    return res.json([])
  }
  res.json(user_categories[0].categories)
})

app.post('/add_product', async (req, res) => {
  const user_id = req.body.user_id
  const product_name = req.body.product_name
  const category = req.body.category_name

  let products_list = await products.find({ user_id: user_id, category: category })

  if (products_list.length < 1) {
    const new_products = new products({
      user_id: user_id,
      product_name: product_name,
      category: category
    })
    new_products.save()

    return res.json([{ product_name: product_name }])
  }

  const new_products = new products({
    user_id: user_id,
    product_name: product_name,
    category: category
  })

  await new_products.save()
  products_list = await products.find({ user_id: user_id, category: category }).select('product_name -_id')

  res.json(products_list)
})

app.post('/delete_product', async (req, res) => {
  const user_id = req.body.user_id
  const product_name = req.body.product_name
  const updated_products = await products.findOneAndRemove({ user_id: user_id, product_name: product_name })
  res.json([])
})

app.get('/get_products', async (req, res) => {
  const user_id = req.query.user_id
  const category = req.query.category

  const user_products = await products.find({ user_id: user_id, category: category })

  if (user_products == undefined) {
    return res.json([])
  }
  res.json(user_products)
})

const generateQR = (table_QR) => {
  return (QRCode.toDataURL(table_QR))
}

app.listen(port, () => {
  console.log("Todo funciona correcto")
})

module.exports = app