const mongoose = require('mongoose')

const url = process.env.MONGO_SERVER || 'mongodb://mongo/bar_owners'
const db = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })


 .then(db => console.log('DB is conencted to', db.connection.host))
 .catch(err => console.error(err))

module.exports = db