const menus = require('../models/menus')
  

class MenuService {

  async addToMenu(data) {

    const user_id = data.user_id

    if (data.first_plates) {
      const first_plate = data.first_plates
      const user_menu = await menus.find({ user_id: user_id })
  
      if (user_menu.length < 1) {
        const new_menu = new menus({
          user_id: user_id,
          first_plates: [first_plate]
        })
        new_menu.save()
  
        return [first_plate]
      }
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $push: { first_plates: first_plate } },
        { new: true })
  
      return updated_menu.first_plates
    }
  
    if (data.second_plates) {
      const second_plate = data.second_plates
      const user_menu = await menus.find({ user_id: user_id })
  
      if (user_menu.length < 1) {
        const new_menu = new menus({
          user_id: user_id,
          second_plates: [second_plate]
        })
        new_menu.save()
  
        return [second_plate]
      }
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $push: { second_plates: second_plate } },
        { new: true })
  
      return updated_menu.second_plates
    }
  
    if (data.desserts) {
      const dessert = data.desserts
      const user_menu = await menus.find({ user_id: user_id })
  
      if (user_menu.length < 1) {
        const new_menu = new menus({
          user_id: user_id,
          desserts: [dessert]
        })
        new_menu.save()
  
        return [dessert]
      }
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $push: { desserts: dessert } },
        { new: true })
  
      return updated_menu.desserts
    }
  
    const other = data.others
    const user_menu = await menus.find({ user_id: user_id })
  
    if (user_menu.length < 1) {
      const new_menu = new menus({
        user_id: user_id,
        others: [other]
      })
      new_menu.save()
  
      return [other]
    }
  
    const updated_menu = await menus.findOneAndUpdate(
      { user_id: user_id },
      { $push: { others: other } },
      { new: true })

        return updated_menu.others
 }


  async deleteItemMenu(data) {
    const user_id = data.user_id

    if (data.first_plates) {
      const first_plate = data.first_plates
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $pull: { first_plates: first_plate } },
        { new: true })
  
      return updated_menu.first_plates
    }
  
    if (data.second_plates) {
      const second_plate = data.second_plates
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $pull: { second_plates: second_plate } },
        { new: true })
  
      return updated_menu.second_plates
    }
  
    if (data.desserts) {
      const dessert = data.desserts
  
      const updated_menu = await menus.findOneAndUpdate(
        { user_id: user_id },
        { $pull: { desserts: dessert } },
        { new: true })
  
      return updated_menu.desserts
    }
  
    const other = req.body.others
  
    const updated_menu = await menus.findOneAndUpdate(
      { user_id: user_id },
      { $pull: { others: other } },
      { new: true })
  }

  async getMenu(user_id) {
    const user_menu = await menus.find({ user_id: user_id })
    if (user_menu == []) {
      return []
    }
    return user_menu
  }
}
module.exports = MenuService




// const updated_menu = await menus.findOneAndUpdate(
//   { user_id: user_id },
//   { $push: { first_plates: first_plate } },
//   { new: true })

// return updated_menu.first_plates
