const tables = require('../models/tables.js')

class TablesService {

  async addTable(user_id, table_name, table_QR){

    const tables_list = await tables.find({ user_id : user_id })
  
    if (tables_list.length < 1){
      const new_tables = new tables({
        user_id: user_id,
        table_name: table_name,
        table_QR: table_QR
      })
      new_tables.save()
      
      return [ table_name ]
    }
    
    const updated_tables = await tables.findOneAndUpdate(
      {user_id : user_id}, 
      {$push:{table_name : table_name, table_QR : table_QR}},
      {new: true})
      
    return updated_tables.table_name
  }

  async deleteTable(user_id, table_name, table_QR) {

    const updated_tables = await tables.findOneAndUpdate(
      { user_id: user_id },
      { $pull: { table_name: table_name, table_QR: table_QR } },
      { new: true })
    
    return updated_tables.table_name
  }

  async getTables(user_id) {
    const tables_list = await tables.find({ user_id: user_id })

    if (tables_list == undefined) {
      return []
    }
    
    return tables_list[0].table_name
  }

}

module.exports = TablesService