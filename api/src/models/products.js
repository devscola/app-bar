const mongoose = require('mongoose')
const Schema = mongoose.Schema

const products_schema = new Schema({
  user_id: String,
  category: String,
  product_name: String
})
module.exports = mongoose.model('products', products_schema)
