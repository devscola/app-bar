const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categories_schema = new Schema({
  user_id: String,
  categories: Array
})
module.exports = mongoose.model('categories', categories_schema)
