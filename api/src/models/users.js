const mongoose = require('mongoose')
const Schema = mongoose.Schema

const users_schema = new Schema({
    bar_name: String,
    email: String,
    password: String,
})
module.exports = mongoose.model('users', users_schema)
