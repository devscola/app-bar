const mongoose = require('mongoose')
const Schema = mongoose.Schema

const menus_schema = new Schema({
    user_id: String,
    first_plates: Array,
    second_plates: Array,
    desserts: Array,
    others: Array
})
module.exports = mongoose.model('menus', menus_schema)