const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tables_schema = new Schema({
    user_id: String,
    table_name: Array,
    table_QR: Array
})
module.exports = mongoose.model('tables', tables_schema)
