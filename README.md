# App-Bar without mentoring!!!!



This application is make to learn REACT


## Table of Contents
  - [System requeriments](#system-requeriments)
  - [Environment variables](#environment-variables)
  - [How to run the application](#how-to-run-the-application)
  - [How to run Unitary tests](#how-to-run-unitary-tests)
    - [E2E tests in console](#e2e-tests-in-console)
    - [E2E tests in browser](#e2e-tests-in-browser)
  - [How to open cypress in a browser](#how-to-open-cypress-in-a-browser)
  - [How to open cypress in console](#how-to-open-cypress-in-console)
  - [Deploy demo](#deploy-demo)


## System requeriments

- Recommend only use the application in Chrome browser.
- Chrome last version  79.0.3945.88.
- Docker-compose version 1.24.1.

## Environment variables

- For use this var make file `.env` in root: `API_URL = http://localhost:8081`.
- For deploy in Heroku we use :
	- `APP_DIR: $CI_PROJECT_DIR/app/`.
  	- `API_DIR: $CI_PROJECT_DIR/api/`.

## How to run the application

- Run: `docker-compose up --build api app`.
- Open in browser: 
  - APP: `localhost:1234`.
  - API: `localhost:8081`.

## How to run  Unitary tests

- Unitary test Jest from Api if docker-compose is up: `docker-compose exec -T api npm run test`.

## How to run Acceptance test

  ### E2E tests in console
    - Run: 
      `cd e2e
        docker run -v $(pwd):/app -w /app node:alpine npm install
      cd ..
        docker run --network="host" -v $(pwd)/e2e:/workdir alvaro78/cypress-bar:browser`.

  ### E2E tests in browser
    - Install Cypress before use the script: in the ee folder type `npm install Cypress`
    - Run:
      - `cd e2e`
      - `./node_modules/.bin/cypress open`

  ### How to open cypress in a browser
    - sh cypress-browser.sh

  ### How to open cypress in console
    - sh cypress-console.sh



## Deploy demo

Currently, we have a  deploy  application for demo purposes, you can check it out at:

- Heroku Cloud Application Plataform:

  - [APP](https://baretos-app.herokuapp.com/ "Complete Aplication").

![Heroku: Cloud Application Platform](https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2016/04/1461122387heroku-logo.jpg)

